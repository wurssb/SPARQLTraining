#!/bin/bash
#============================================================================
#title          :Main installation
#description    :Documentation and docker building script
#author         :Jasper Koehorst
#date           :2018
#version        :0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest
# git -C "$DIR" pull
cd $DIR

# Prepares documentation
./markdown/build.sh

# Downloads data files from public server
wget -r -np -nc -nH --cut-dirs=2 http://fungen.wur.nl/\~jasperk/SPARQLTraining/data/

# Download nextprot

# Builds the docker image with the documentation, graph database and content
docker build -t wurssb/sparqltraining .
docker push wurssb/sparqltraining
