#!/bin/bash
#============================================================================
#title          :Builder
#description    :Website and PDF builder
#author         :Jasper Koehorst
#date           :2017
#version        :0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR

docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material build
