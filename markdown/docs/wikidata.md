## Querying WikiData


Wikidata is a free and open knowledge base that can be read and edited by both humans and machines.
Wikidata acts as central storage for the structured data of its Wikimedia sister projects including Wikipedia, Wikivoyage, Wikisource, and others.

Wikidata also provides support to many other sites and services beyond just Wikimedia projects! The content of Wikidata is available under a free license, exported using standard formats, and can be interlinked to other open data sets on the linked data web.

All data can be accessed through the public sparql endpoint located at [https://query.wikidata.org](https://query.wikidata.org)

At the SPARQL endpoint page you can find various example queries covering a wide range of topics.

If you prefer another interface for querying you can access the SPARQL endpoint by using a so called 
***Federated Query***

```SPARQL
    PREFIX wikibase: <http://wikiba.se/ontology#>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX bd: <http://www.bigdata.com/rdf#>
    SELECT * 
    WHERE { 
        SERVICE <https://query.wikidata.org/sparql> { 
            SELECT ?instance_of ?instance_ofLabel ?NCBI_Taxonomy_ID WHERE {
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
              ?instance_of wdt:P31 wd:Q855769.
              OPTIONAL { ?instance_of wdt:P685 ?NCBI_Taxonomy_ID. }
            }
            LIMIT 100
        }
    }
```
    
You normally use federated queries to connect data from your own triple store with data from a remote sparql endpoint. You can of course also only query the endpoint and ignore your own data as shown in the query above.

###Some tips

* Use CTRL + SPACE for autocompletion and help with searching the right properties

* wdt: (for predicates unless wanted otherwise)
* wd: (subject / object from items)

* The SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } is a handy property to show the labels of ?something when typed as ?somethingLabel

* Due to the vast resource and dynamic nature there is not really a map of wikidata connections… But you can easily browse and search through wikidata pages

![](images/wikidata.png)


### Exercises
-----

#### General

???+ note "All items that are a statue"

    ```SPARQL
	SELECT ?item WHERE {
	  ?item wdt:xxx wd:xxx .
	} LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT ?item WHERE {
            ?item wdt:P31 wd:Q179700 .
        } LIMIT 10
        ```

???+ note "All items that are a statue and the corresponding label"
    
    Using `SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }`
    you can receive the label of a variable by selecting for ?xxxLabel
    
    ```SPARQL
	SELECT ?item WHERE {
	  ?item wdt:xxx wd:xxx .
	  ...
	} LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT ?item ?itemLabel WHERE {
          ?item wdt:P31 wd:Q179700 .
          SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        } LIMIT 10
        ```

???+ note "Where are the statues located on a MAP?"
    
    Using the 'eye' icon just above the query results you can change the view. Try map when you have found a way to retrieve the coordinates.
    
    ```SPARQL
	SELECT ?item ?itemLabel ?loc WHERE {
	  ?item wdt:xxx wd:xxx .
	  ...
	} LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT ?item ?itemLabel ?loc WHERE {
          ?item wdt:P31 wd:Q179700 .
          ?item wdt:P625 ?loc .
          SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        } LIMIT 10
        ```
        
???+ note "Which do not have a picture?"
    
    In some way you have to make sure that the results do not contain entries that have a picture    
    ```SPARQL
	SELECT ?item ?itemLabel ?loc WHERE {
	  ?item wdt:xxx wd:xxx .
	  ...
	} LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT ?item ?itemLabel ?loc WHERE {
          ?item wdt:P31 wd:Q179700 .
          ?item wdt:P625 ?loc .
          MINUS { ?item wdt:P18 ?image . }
          SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        } LIMIT 100
        ```

???+ note "Which do not have a picture and are in the netherlands??"
    
	You can obtain all statues and use the map view to identify the items. However this will also retrieve data points outside the Netherlands. Lets save the machine some time and only receive the ones that are in the netherlands.
	
    ```SPARQL
	SELECT ?item ?itemLabel ?loc WHERE {
	  ?item wdt:xxx wd:xxx .
	  ...
	} LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT DISTINCT ?item ?itemLabel ?loc WHERE {
          ?item wdt:P31 wd:Q179700 .
          ?item wdt:P625 ?loc .
          ?item wdt:P17 ?country .
          ?country rdfs:label ?label .
          FILTER(CONTAINS(?label, "Nederland"@nl)) 
          MINUS { ?item wdt:P18 ?image . }
          SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        }
        ```

???+ note "Who doesnt like cats?"
    
	Obtain all cats and make a picture wall of it! (Use the eye icon)
		
    ```SPARQL
	SELECT ?item ?itemLabel ?image WHERE {
	  ?item wdt:xxx wd:xxx .
	  ...
	} LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT ?item ?itemLabel ?image
        WHERE {
          ?item wdt:P31 wd:Q146.
          ?item wdt:P18 ?image .
          SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        }
        ```
        
#### Biology related
-----

???+ note "All strains with a taxonomic number"
    
    - Predicates: 
        - wdt:P685 = NCBI Taxonomy ID
        - wdt:P31 = IS A
    - Objects:
        - wd:Q855769 = Micro-organism

    ```SPARQL
    SELECT *
    WHERE {
        ?... wdt:... ?... .
        ?item wdt:P31 wd:Q855769 .
    } LIMIT 10
    ```

    ??? warning "Answer"
        ```SPARQL
        SELECT *
        WHERE {
            ?item wdt:P685 ?rank .
            ?item wdt:P31 wd:Q855769 .
        } LIMIT 10
        ```

???+ note "All organisms with a taxonomic number but now with labels"

    ```SPARQL
    SELECT ?item ?itemLabel ?rank ?rankLabel #rank is already a number
    WHERE {
    ?... wdt:... ?... .
    ?item wdt:P31 wd:Q855769 .
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    } LIMIT 10
    ```
    
    ??? warning "Answer"
        ```SPARQL
        SELECT ?item ?itemLabel ?rank ?rankLabel
        WHERE {
            ?item wdt:P685 ?rank .
            ?item wdt:P31 wd:Q855769 .
            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        } LIMIT 10
        ```

???+ note "All organisms that have a gram staining property"

    ```SPARQL
    SELECT ?item ?itemLabel ?rank ?gram ?gramLabel
    WHERE {
        ?item wdt:P685 ?rank .
        ?item wdt:P31 wd:Q855769 .
        ?... wdt:... ?... .
        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }
    ```
    
    ??? warning "Answer"
    
        ```SPARQL
        SELECT ?item ?itemLabel ?rank ?gram ?gramLabel
        WHERE {
            ?item wdt:P685 ?rank .
            ?item wdt:P31 wd:Q855769 .
            ?item wdt:P2597 ?gram .
            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        }
        ```


    

???+ note "All proteins (in this link you can see that found in taxon does the coupling) belonging to Pseudomonas putida KT2440 (You can use as filter the GenBank Assembly accession property or taxon name)."

    ```SPARQL
    SELECT ?item ?itemLabel ?rank ?refseq ?protein ?proteinLabel 
    WHERE {
     ?item wdt:P685 ?rank .
     ?item wdt:P31 wd:Q855769 .
     ?item wdt:P4333 "GCA_......2" .
     ?item wdt:P2249 ?refseq .
     SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
     
     ?protein wdt:.... ?item .
    }
    ```

    ??? warning "Answer"
        ```SPARQL
        SELECT ?item ?itemLabel ?rank ?refseq ?protein ?proteinLabel 
        WHERE {
         ?item wdt:P685 ?rank .
         ?item wdt:P31 wd:Q855769 .
         ?item wdt:P4333 "GCA_000007565.2" .
         ?item wdt:P2249 ?refseq .
         SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
         
         ?protein wdt:P703 ?item .
        }
        ```

???+ note "All human proteins that are inside the mitochondria and can bind metal ions"
    ```SPARQL
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX wp: <http://vocabularies.wikipathways.org/wp#>
    SELECT DISTINCT ?item ?itemLabel  WHERE {
        ?item wdt:P352 ?uniprotid .
        ?item wdt:... wd:... .
        ?item wdt:... wd:... .
        ?item wdt:... wd:... .
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
    }
    ```

    ??? warning "Answer"
        ```SPARQL
        PREFIX dcterms: <http://purl.org/dc/terms/>
        PREFIX wp: <http://vocabularies.wikipathways.org/wp#>
        SELECT DISTINCT ?item ?itemLabel  WHERE {
            ?item wdt:P352 ?uniprotid .
            ?item wdt:P703 wd:Q15978631 .
            ?item wdt:P681 wd:Q39572 .
            ?item wdt:P680 wd:Q13667380 .
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        }
        ```
     
     
     
###WIKIDATA & UNIPROT

**Perform a Wikidata query from the UniProt database (do not forget the prefixes!!)**

???+ note "All human proteins that are inside the mitochondria and can bind metal"
    ```SPARQL
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    SELECT DISTINCT ?item  WHERE {
        SERVICE <http://query.wikidata.org/sparql> {
            ?item wdt:P352 ?uniprotid .
    ?item wdt:... wd:... .
    ?item wdt:... wd:... .
    ?item wdt:... wd:... .
    }
    }
    ```
    
    ??? warning "Answer"
    	```SPARQL
	    PREFIX wd: <http://www.wikidata.org/entity/>
	    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
	    SELECT DISTINCT ?item  WHERE {
	        SERVICE <http://query.wikidata.org/sparql> {
	            ?item wdt:P352 ?uniprotid .
	            ?item wdt:P703 wd:Q15978631 .
	            ?item wdt:P681 wd:Q39572 .
	            ?item wdt:P680 wd:Q13667380 .
	        }
	    }
	    ```

???+ note "To couple the results, we need an **exactMatch!** Add this to the query otherwise we do not have a common link between the two databases"

	??? warning "Answer"
	    ```SPARQL
	    PREFIX wd: <http://www.wikidata.org/entity/>
	    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
	    SELECT DISTINCT *  WHERE {
	        SERVICE <http://query.wikidata.org/sparql> {
	            ?item wdt:P352 ?uniprotid .
	            ?item wdt:P703 wd:Q15978631 .
	            ?item wdt:P681 wd:Q39572 .
	            ?item wdt:P680 wd:Q13667380 .
	            ?item wdt:P2888 ?uniprotURL .
	        }
	    }
	    ```
 

???+ note "Are any of these enzymes? (Now we use uniprot for this) and look at a previous query for the enzyme part. Do not forget the up prefix…. ORDER IS **IMPORTANT!** First WikiData then Uniprot…"

	??? warning "Answer"
	    ```SPARQL
	    PREFIX up:<http://purl.uniprot.org/core/> 
	    PREFIX wd: <http://www.wikidata.org/entity/>
	    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
	    SELECT DISTINCT *  WHERE {
	        SERVICE <http://query.wikidata.org/sparql> {
	            ?item wdt:P352 ?uniprotid .
	            ?item wdt:P703 wd:Q15978631 .
	            ?item wdt:P681 wd:Q39572 .
	            ?item wdt:P680 wd:Q13667380 .
	            ?item wdt:P2888 ?uniprotURL .
	        }
	        ?uniprotURL up:enzyme ?enzyme .
	    }
	    ```


##Use your imagination…
Are there any interesting properties in UniProt or WikiData that you would like to query? Or do you want to dive further into GBOL and see what the possibilities are?

Try it! And perhaps we can show case some new examples…
