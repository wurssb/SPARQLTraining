# Querying through an API

## R

In order to run a query in R, one first needs to install the packages required. This can be done using the follwing commands in R:-

```R
install.packages(“RCurl”)
install.packages(“XML”)
install.packages(“SPARQL”)
```

The packages ‘RCurl’ and ‘XML’ must be installed first as they are pre-requisites of the ‘SPARQL’ package.

We can  now begin to query in R using ‘SPARQL’. Let us have a look at an example query.

Query 1:- Getting the list of genomes in the database.

###loading the required library to execute a SPARQL query.

```R
library("SPARQL")
```

###creating a variable which stores the query as a character.

```R
samples <- "
PREFIX gbol:<http://gbol.life/0.1/>
SELECT ?sample
WHERE {
  ?sample a gbol:Sample .
}"
```


"
###setting up the access-point.

```R
endpoint <- "http://sparql.systemsbiology.nl/repositories/GBOL"
```

###storing the output of the query.

```R
output_1 <- SPARQL(url = endpoint, query = samples, ns = c("gbol","http://gbol.life/0.1/"))
```

The ouput obtained after running the query is a list with two components. The first component is a data-frame which consists of the data which was retireved by the query. And the second component is the name-space from where the data was obatined. 

In the above line it is seen that the function SPARQL is used from the package ‘SPARQL’. Two most essential arguments which must be given to execute a sparql-query are ‘url’ and ‘query’. The ‘url’ is the access-point from where the data needs to be retrieved and ‘query’ is the sparql-query to be executed. The argument ‘ns’ is acronym for ‘name-space’.

So, after running a sparql query we need to retrieve the data in some particular format to carry out our required analysis. For that we have to access the first component of the list as discussed earlier.

###Accessing the data

```R
my_samples <- output_1$results
```

On running this line of code we get a data-frame called ‘my_samples’ with multiple rows and one column.

###In summary:

```R
library("SPARQL")

samples <- "
PREFIX gbol:<http://gbol.life/0.1/>
SELECT ?sample
WHERE {
  ?sample a gbol:Sample .
}"

endpoint <- "http://sparql.systemsbiology.nl/repositories/GBOL"

output_1 <- SPARQL(url = endpoint, query = samples, ns = c("gbol","http://gbol.life/0.1/"))

my_samples <- output_1$results
```

### Biological analysis

You can easily use the data stored in the SPARQL endpoint for all kind of biological analysis.

For example, to create a phylogenetic interpretation of the functionalities in a group of genomes, the following codeset can be applied.

```R
library("SPARQL")
library("reshape2")
library("ggrepel")
library("ggplot2")

samples <- "
PREFIX gbol:<http://gbol.life/0.1/>
SELECT DISTINCT ?sample ?accession ?db
WHERE {
?contig gbol:sample ?sample .
?contig gbol:feature ?gene .
?gene a gbol:Gene . 
?gene gbol:transcript ?mrna .
?mrna gbol:feature ?cds .
?cds gbol:protein ?protein .
?protein gbol:xref ?xref .
?xref gbol:db ?db .
?xref gbol:accession ?accession .
?xref gbol:db <http://gbol.life/0.1/db/pfam> .
}"

endpoint <- "http://sparql.systemsbiology.nl/repositories/GBOL"

output_1 <- SPARQL(url = endpoint, query = samples, ns = c("gbol","http://gbol.life/0.1/"))

my_samples <- output_1$results

### Turn this into a binary matrix

matrix = xtabs(~ sample + accession, my_samples)
matrix = acast(as.data.frame(matrix), sample~accession, value.var="Freq")

### Calculating PCA

pca = prcomp(matrix)
pcadf = as.data.frame(pca$x)

### Plot results
p<-ggplot(pcadf,aes(x=PC1,y=PC2, label=rownames(pcadf)))
p<-p+geom_point() +geom_text_repel() #+geom_text(aes(label=rownames(pcadf)),hjust=0, vjust=0)
p
```

![](./images/pcaplot.png)



## Python

To use SPARQL in python you should use [SPARQLWrapper](https://rdflib.github.io/sparqlwrapper/)

```python
from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setQuery("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?label
    WHERE { <http://dbpedia.org/resource/Asturias> rdfs:label ?label }
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

for result in results["results"]["bindings"]:
	print(result["label"]["value"])
```
