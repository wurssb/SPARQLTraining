## Understanding the basics

To understand the basics of SPARQL and directly querying it we are going to use a very basic database.
This database, developed by the Swiss Bioinformatics Institute, consists of the following elements:

![](images/basicModel.png)

This database consists of a large variety of creatures. Some are further classified as Persons while others are falling under the Animal category.

SPARQL/RDF work according triple statements as directed by the arrows.

For example **John** is of **Type** **Person**.

To reduce the amount of typing in this exercise these are the prefixes used:

```SPARQL
PREFIX dbo:<http://dbpedia.org/ontology/>
PREFIX dbp:<http://dbpedia.org/property/>
PREFIX dbpedia:<http://dbpedia.org/resource/>
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX tto:<http://example.org/tuto/ontology#>
PREFIX ttr:<http://example.org/tuto/resource#>
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
```

*These prefixes are to be placed at the top of each query executed and in GraphDB should be automatically added upon writing the queries*

### Exercises

**First open the SPARQL training image in your browser.**

1. Check your command line terminal for the exact location of the SPARQL training image
(will look something like http://localhost:7300#).
2. The graphDB webbrowser will start up.
3. Select a repository by selecting `setup` and then `Repositories` in the left side-menu.
4. For this exercise we will connect with the `BASIC` repository. <br>
&nbsp;&nbsp;Connecting is done by clicking the plug icon at the left hand side of the repository names.

5. After this, select `SPARQL` on the left side-menu to obtain the SPARQL interface from GraphDB.
6. We can start by executing the SPARQL queries introduced below

When executing a query, all URLs (in blue) can be clicked upon which gives additional information and gives insights into what other properties can be directly queried from this statement.

???+ note "Select all things that are Persons..."

    Selects subjects connected to the object dbo:Person via the predicate rdf:type ?thing is the only variable

	```SPARQL
	SELECT ?...
	WHERE {
		?person rdf:type dbo:.... .
	}
	```
	
	??? warning "Answer"
	
		```SPARQL
		SELECT ?person
		WHERE {
 			?person rdf:type dbo:Person .
		}
		```
		
		This query will retrieve all things in the database that are connected to the type Person as defined by the dbo ontology

???+ note "Select all persons that are Female..."

    Using the results from the previous query you can browse to get more information. For example when clicking on John, it shows that it has a birthdate, a name and ....

	??? warning "Answer"
	
		```SPARQL
		SELECT ?person
		WHERE {
		   ?person rdf:type dbo:Person .
  			?person tto:sex "female" .
		}

		```


???+ note "What does this query do?..."

	```SPARQL
	SELECT ?thing
	WHERE {
	  ?thing tto:sex "female" .
	}
	```


	??? warning "Answer"
		This will return everything that is female including the LunaCat
	
???+ note "Select persons and their pets..."

    In this query we ask for more than one thing. Namely a person and their pets.

	??? warning "Answer"
	
		```SPARQL
		SELECT ?person ?pet
		WHERE {
		   ?person rdf:type dbo:Person .
			?person tto:pet ?pet .
		}
		```
		
		In the select statement we now have two variables. ?person, representing everything of type Person and ?pet. As you can see ?pet is linked to ?person in the second statement.
		
		
???+ note "Select persons and if they have one, their pets..."

    In this case we do not want to be very strict. We would like to know who has a pet and who does not.

	??? warning "Answer"
	
		```SPARQL
		SELECT ?person ?pet
		WHERE {
			?person rdf:type dbo:Person .
			optional { ?person tto:pet ?pet }.
		}
		```
		
		Nearly identical to the previous statement but here we use the **OPTIONAL** around one or more statements.
		

		
???+ note "Who does not have a pet?..."

    Which persons do not have a pet using a **filter** and **not exists**

	??? warning "Answer"
	
		```SPARQL
		SELECT ?person ?pet
		WHERE {
		   ?person rdf:type dbo:Person .
			FILTER NOT EXISTS {?person tto:pet ?pet }.
		}

		```
		
		Again we first select the persons in the database and through the **filter** we return all persons that do not have this statement.
		
		
???+ note "Which direct subclassess are connected to a creature..."

    As can be seen in the figure there are classess who are subclass of other classess. For example a Person is a subclass of a Creature. These subclass relations are connected via rdfs:subClassOf a standard in SPARQL and RDF.

	??? warning "Answer"
	
		```SPARQL
		SELECT ?subSpecies
		WHERE {
		  ?subSpecies rdfs:subClassOf tto:Creature .
		}
		```
		
		This returns the first level of subclassess that are connected to the Creature class.
		
		
???+ note "Which subclassess are all connected to a creature..."

    As can be seen in the figure there are also classess connected to the subclassess of creature. For example a Cat is a subclass of Animal which is a subclass of a Creature. These subclass relations are connected via rdfs:subClassOf a standard in SPARQL and RDF.
    
    There are different ways to express the property path level
	
	
	  - path* -> means 0 or more
	  - path+ -> means 1 or more
	  - path? -> means 0 or 1 

	??? warning "Answer"
	
		```SPARQL
		SELECT ?subSpecies
		WHERE {
			?subSpecies rdfs:subClassOf+ tto:Creature .
		}
		```
		
		This returns the first level of subclassess that are connected to the Creature class.
		

???+ note "Select all animals..."

    When looking at the figure you can see that Cat, Dog and Monkey are a subclassof Animal. These subclassof relations are in place to group one or more classess to a parent class. 

	??? warning "Answer"
	
		```SPARQL
		SELECT ?thing ?type
		WHERE {
		  ?type rdfs:subClassOf+ tto:Animal .
		  ?thing a ?type .
		}
		```
		
		We ask for many kind of animals here. Cat, Dog and Monkey all part of the Animal class. To get the parent class of a Person/Cat/Dog... you use rdfs:subClassOf. As a class can be a subclassof another class which in turn can be a class of another subclass you can use a + in your query. This will retrieve all entries that in some distance a connection to Animal. 
		
???+ note "Select the grandfather of Eve"

	Try to obtain the grandfather of Eve through the dbo:parent property. 

	```SPARQL
	SELECT ?grandfather
	WHERE {
		ttr:Eve dbo:parent  .... .
		...   ...  ?grandfather  .
	}
	```

	??? warning "Answer"
	
		```SPARQL
		SELECT ?grandfather
		WHERE {
			ttr:Eve dbo:parent  ?parent .
			?parent dbo:parent  ?grandfather  .
		}
		```
		
		Here we use the dbo:parent twice to retrieve the father of the father
		
		
???+ note "Select the grandfather of Eve using SequencePaths"

	The previous query as shown here can be more simplified using SequencePaths. Try to combine the two statements using a "/"

	??? warning "Answer"
	
		```SPARQL
		SELECT ?grandfather
		WHERE {
			ttr:Eve dbo:parent / dbo:parent ?grandfather  .
		}
		```
		
		Here we use the dbo:parent twice without any variable in between. This tells sparql that the "object" of Eve is to be used directly as a subject in the next dbo:parent statement. 		


### Advanced exercises

???+ note "What does the following query do?"
	
	```SPARQL
	select *
	WHERE {
	  ?creature dbp:name ?name .
	  FILTER ( REGEX(?name, "^[RI].*x$" ) )
	}
	```
	
	[REGEX cheat sheet](https://i.imgur.com/UTlGckN.png)
	
	??? warning "Answer"
		**Select creature names starting with either R or I and ending by a x**
		
	    Here we search for Persons or Animals that have a certain naming format. Using a combination of FILTER and REGEX (Regular Expressions) we can filter names in the database. We first retrieve the names of all the entries and then we filter the name according to the regular expression. 


???+ note "Filter for a particular weight range (5 - 7 kg)..."

    Using the tto:weight we can retrieve the weight value and use FILTER to select a range.
    
	```SPARQL
	SELECT ?thing ?weight
	WHERE {
		?thing tto:.... ?weight .
		FILTER (?weight > ... && ?weight < ...) 
	} ORDER BY ?...
	```

	??? warning "Answer"
	
		```SPARQL
		SELECT ?thing ?weight
		WHERE {
			?thing tto:weight ?weight .
			FILTER (?weight > 5 && ?weight < 7.0) 
		} ORDER BY ?weight
		```
		
		Using the && symbols you can apply a double filter here. First we filter for all the weights abouve 5kg and secondly for all the weights below 7kg.
		
		
		
		

<!--???+ note "Template title..."

    Template explain

	??? warning "Answer"
	
		```
		QUERY HERE
		```
		
		Query explained-->