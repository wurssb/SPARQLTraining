#Glossary

####Accessible 
Stored for long term so that the data can easily be accessed and/or downloaded with well-defined license and access conditions (open access when possible), whether at the level of metadata, or at the level of the actual data.

####Endpoint 
A SPARQL endpoint is a service where data is stored, which enables users (human or computer) to query a knowledse database through the SPARQL language.

####FAIR Principles 
*Findability, Accessibility, Interoperability, and Reusability*. 
These principles serve to guide data producers and publishers as they navigate around these obstacles, thereby helping to maximize the added-value gained by contemporary, formal scholarly digital publishing. Importantly, it is the intent that the principles apply not only to ‘data’ in the conventional sense, but also to the algorithms, tools, and workflows that led to that data. All scholarly digital research objects — from data to analytical pipelines — benefit from application of these principles, since all components of the research process must be available to ensure transparency, reproducibility, and reusability.

####Federated Query 
An information retrieval technology that allows the simultaneous search of multiple searchable resources.
Combining information from datasets that live in different places and have different SPARQL endpoints.

####Findable 
Easy to find for both humans and computers, with metadata that facilitate searching for specific datasets.

####GraphDB 
GraphDB is an enterprise ready Semantic Graph Database, compliant with W3C Standards. Semantic graph databases (also called RDF triplestores) provide the core infrastructure for solutions where modelling agility, data integration, relationship exploration and cross-enterprise data publishing and consumption are important. 

####Interoperable 
The ability of data or tools from non-cooperating resources to integrate or work together with minimal effort.

####Linked Data 
A method of publishing structured data so that it can be interlinked and become more useful through semantic queries. It builds upon standard Web technologies such as HTTP, RDF and URIs, but rather than using them to serve web pages for human readers, it extends them to share information in a way that can be read automatically by computers.

####Object 
The object is the actual value of the triple.

####Ontology 
On the Semantic Web, ontologies or vocabularies define the concepts and relationships (also referred to as “terms”) used to describe and represent an area of concern. Ontologies/vocabularies are used to classify the terms that can be used in a particular application, characterize possible relationships, and define possible constraints on using those terms. In practice, ontologies/vocabularies can be very complex (with several thousands of terms) or very simple (describing one or two concepts only).

####OWL 
The Web Ontology Language is a Semantic Web language designed to represent rich and complex knowledge about things, groups of things, and relations between things. OWL facilitates greater machine interpretability of Web content than that supported by XML, RDF, and RDF Schema (RDF-S) by providing additional vocabulary along with a formal semantics.

####Predicate 
The predicate defines the piece of data in the object we are giving a value to.

####Provenance 
It means the chronology of the ownership, custody or location of a historical object. In semantics it contains the history of where it comes from, which is important for reproducibility. The provenance of information is crucial to making determinations about whether information is trusted, how to integrate diverse information sources, and how to give credit to originators when reusing information.

####Query 
A query is a request for data or information from a database table or combination of tables

####Relationship 
The description of which connects subject and object to eachother

####Reusable 
Ready to be used for future research and to be further processed using computational methods.

####RDF 
Resource Description Framework; a globally-accepted framework for data and knowledge representation that is intended to be read and interpreted by machines.

####Triple Store
A triplestore or RDF store is a purpose-built database for the storage and retrieval of triples through semantic queries. A triple is a data entity composed of subject-predicate-object. Much like a relational database, one stores information in a triplestore and retrieves it via a query language. Unlike a relational database, a triplestore is optimized for the storage and retrieval of triples. In addition to queries, triples can usually be imported/exported using Resource Description Framework (RDF) and other formats.

####Triples
The smallest irreducible representation for binary relationship represented as *subject-predicate-object*. 
John | Is a Friend of | James <br>
James | Is a friend of | Jill <br>
Jill | Likes | Snowboarding <br>
Snowboarding | Is a | Sport <br>

####Semantics
Semantics (from Ancient Greek: σημαντικός sēmantikós, "significant") is the linguistic and philosophical study of meaning, in language, programming languages, formal logics, and semiotics. It is concerned with the relationship between signifiers—like words, phrases, signs, and symbols—and what they stand for, their denotation.

####Semantic web
The Semantic Web is an extension of the World Wide Web through standards by the World Wide Web Consortium (W3C). The standards promote common data formats and exchange protocols on the Web, most fundamentally the Resource Description Framework (RDF). According to the W3C, "The Semantic Web provides a common framework that allows data to be shared and reused across application, enterprise, and community boundaries". The Semantic Web is therefore regarded as an integrator across different content, information applications and systems.

####SPARQL
SPARQL Protocol And RDF Query Language is an RDF query language, that is, a semantic query language for databases, able to retrieve and manipulate data stored in Resource Description Framework (RDF) format.

####Subject
The subject is, well, the subject. It identifies what object the triple is describing.

####World Wibe Web Consortium (W3C) 
The main international standards organization for the World Wide Web that develops interoperable technologies (specifications, guidelines, software, and tools), to lead the Web to its full potential.