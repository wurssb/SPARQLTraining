## Querying UniProt


UniProt is a freely accessible database of protein sequence and functional information. It contains a large amount of information about the biological function of proteins derived from the research literature. All this data has been converted into RDF. Currently there are more than 34,773,801,346 triples in the database (release 2017_12). 

**Only a small subset is available through the GraphDB interface as it would not fit on a regular machine.**

All data can be accessed through the public sparql endpoint located at [http://sparql.uniprot.org/sparql](http://sparql.uniprot.org/sparql)

An oveview of the data structures can be found at [http://sparql.uniprot.org/.well-known/void](http://sparql.uniprot.org/.well-known/void) and a small subset is shown in the following image.

![Uniprot (sub-)overview](images/uniprot.jpg)

At the SPARQL endpoint page you can find various example queries covering a wide range of topics.

If you prefer the GraphDB interface for querying you can access the SPARQL endpoint by using a so called *Federated Query*

```SPARQL
PREFIX up:<http://purl.uniprot.org/core/> 
SELECT * 
WHERE { 
    SERVICE <https://sparql.uniprot.org/sparql> { 
        SELECT ?taxon
        WHERE {
            ?taxon a up:Taxon .
        }
    }
}
```
	
You normally use federated queries to connect data from your own triple store with data from a remote sparql endpoint. You can of course also only query the endpoint and ignore your own data as shown in the query above.

### Exercises

???+ note "Get all taxons and the scientific name that belongs to it"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT ?taxon ?name
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:...... ?name .
	}
	```

	??? warning "Answer"

		```SPARQL
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT ?taxon ?name
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
		}
		```
	
???+ note "Filter or match for	**Lokiarchaeum sp. GC14_75**"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
			SELECT ?taxon ?name
			WHERE
			{
					?taxon a up:Taxon .
			  		?taxon up:...... ?name .
					FILTER(?name = "......")
			}
	```
	
	??? warning "Answer"
	
		```SPARQL
			PREFIX up:<http://purl.uniprot.org/core/> 
			SELECT ?taxon ?name
			WHERE
			{
					?taxon a up:Taxon .
			  		?taxon up:scientificName ?name .
					FILTER(?name = "Lokiarchaeum sp. GC14_75")
			}
		```


???+ note "Select all proteins for this organism"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT *
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:...... ?name .
			FILTER(......)
	  		?uniprot up:organism ?taxon .
	  		?uniprot a up:...... .
	} 
	```
	
	??? warning "Answer"
		```SPARQL
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT *
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		} 
		```

 
???+ note "What is the parent taxon of this organism and what is the rank? (rdfs:subClassOf)"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT DISTINCT *
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:scientificName ?name .
			FILTER(?name = "Lokiarchaeum sp. GC14_75")
	  		?taxon up:...... ?rank .
	  		# The reasoner automatically retrieves everything of subClassOf+ relationship
	  		?taxon rdfs:...... ?parent .
	  		?parent up:...... ?parentRank .
	}
	```

???+ note "How many proteins does this organism have?"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT (COUNT(DISTINCT(?......)) AS ?pcount)
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:...... ?name .
			FILTER(?name = "Lokiarchaeum sp. GC14_75")
	  		?uniprot up:organism ?taxon .
	  		?uniprot a up:...... .
	}
	```
	
	??? warning "Answer"

		```SPARQL
			PREFIX up:<http://purl.uniprot.org/core/> 
			SELECT (COUNT(DISTINCT(?uniprot)) AS ?pcount)
			WHERE
			{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
			}
		```


**Cross-references (links to external databases) !!! See documentation**

???+ note "Via seeAlso which annotation databases are available?"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT DISTINCT ?db
	WHERE
	{
			?taxon a up:...... .
	  		?taxon up:...... ?name .
			FILTER(?name = "Lokiarchaeum sp. GC14_75")
	  		?uniprot up:organism ?taxon .
	  		?uniprot a up:...... .
	  		?uniprot rdfs:...... ?also .
	  		?also up:database ?db .
	}
	```
	
	??? warning "Answer"
	
		```SPARQL
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT DISTINCT ?db
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		  		?uniprot rdfs:seeAlso ?also .
		  		?also up:database ?db .
		}
		```


 
???+ note "What is the most abundant PFAM in this organism across all proteins?"

	??? warning "Answer"
		
		```SPARQL
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT DISTINCT ?seeAlso (COUNT(?seeAlso) AS ?count)
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		  		?uniprot rdfs:seeAlso ?seeAlso .
		  		?seeAlso up:database ?db .
		} GROUP BY ?seeAlso
		ORDER BY DESC(?count)
		```


???+ note "Select all enzymes (proteins with an EC) for this organism"

	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT *
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:...... ?name .
			FILTER(?name = "Lokiarchaeum sp. GC14_75")
	  		?uniprot up:organism ?taxon .
	  		?uniprot a up:...... .
	  		?uniprot up:...... ?enzyme .
	}
	```
	
	??? warning "Answer"
		
		```SPARQL
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT *
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		  		?uniprot up:enzyme ?enzyme .
		}
		```


???+ note "And how many are there?"
	
	By using COUNT we can count how many unique protein ids are present in the previous query
	
	```SPARQL
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT (....(DISTINCT(?....)) AS ?count)
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:scientificName ?name .
			FILTER(?name = "Lokiarchaeum sp. GC14_75")
	  		?uniprot up:organism ?taxon .
	  		?uniprot a up:Protein .
	  		?uniprot up:enzyme ?enzyme .
	}
	```
	
	??? warning "Answer"

		```SPARQL
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT (COUNT(distinct(?uniprot)) AS ?count)
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		  		?uniprot up:enzyme ?enzyme .
		}
		```
 


**These are tricky ones… You can also play with WikiData…**

???+ note "Where are the enzymes located?"

	```SPARQL
	PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT DISTINCT *
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:...... ?name .
			FILTER(?name = ......)
	  		?uniprot up:organism ?taxon .
	  		?...... a up:Protein .
	  		?uniprot up:...... ?enzyme .
	  		?uniprot up:annotation ?annot .
	  		?annot a up:Subcellular_Location_Annotation .
	  		?annot up:locatedIn ?in .
	  		?in up:cellularComponent ?component .
	  		?component skos:prefLabel ?label .
	}
	```
	
	??? warning "Answer"
		```SPARQL
		PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT DISTINCT *
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		  		?uniprot up:enzyme ?enzyme .
		  		?uniprot up:annotation ?annot .
		  		?annot a up:Subcellular_Location_Annotation .
		  		?annot up:locatedIn ?in .
		  		?in up:cellularComponent ?component .
		  		?component skos:prefLabel ?label .
		}
		```

???+ note "Which enzymes are located on the 'Cell membrane'"
	
	```SPARQL
	PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
	PREFIX up:<http://purl.uniprot.org/core/> 
	SELECT DISTINCT *
	WHERE
	{
			?taxon a up:Taxon .
	  		?taxon up:scientificName ?name .
			FILTER(?name = "Lokiarchaeum sp. GC14_75")
	  		?uniprot up:organism ?taxon .
	  		?uniprot a up:Protein .
	  		?uniprot up:enzyme ?enzyme .
	  		?uniprot up:annotation ?annot .
	  		?annot a up:...... .
	  		?annot up:locatedIn ?in .
	  		?in up:...... ?component .
	  		?component skos:prefLabel ?label .
	  		VALUES ?label { "Cell ......" }
	}
	```
	
	??? warning "Answer"
	
		```SPARQL
		PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
		PREFIX up:<http://purl.uniprot.org/core/> 
		SELECT DISTINCT *
		WHERE
		{
				?taxon a up:Taxon .
		  		?taxon up:scientificName ?name .
				FILTER(?name = "Lokiarchaeum sp. GC14_75")
		  		?uniprot up:organism ?taxon .
		  		?uniprot a up:Protein .
		  		?uniprot up:enzyme ?enzyme .
		  		?uniprot up:annotation ?annot .
		  		?annot a up:Subcellular_Location_Annotation .
		  		?annot up:locatedIn ?in .
		  		?in up:cellularComponent ?component .
		  		?component skos:prefLabel ?label .
		  		VALUES ?label { "Cell membrane" }
		}
		```