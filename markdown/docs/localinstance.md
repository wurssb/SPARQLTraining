## Running SPARQLTraining locally

To run the SPARQL interface and Manual used in this training exercise on your own machine / laptop the docker application is required.

[Docker](https://www.docker.com/) is a modern version of a virtual machine and can do much more than what we show here. To obtain docker for your machine (windows, linux, mac) go to the mentioned website and download the application.

After installation docker should be available from the command line:

```
> docker

Usage:	docker COMMAND

A self-sufficient runtime for containers

Options: ...
```

If this all works fine we can run docker images...

###Starting SPARQLTraining
The SPARQL database and documentation can be started using the following command:

    docker run -p 8080:80 -p 7300:7200 wurssb/sparqltraining

This will start the documentation at [localhost:8080](http://localhost:8080) and the database can be accessed at [localhost:7300](http://localhost:7300)

###Managing your instances
To in general manage your docker instances when running multiple different dockers or to manage long forgotten docker images and to easily manage them run the following docker instance:

`docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer`

This will start a managment environment at [http://localhost:9000](http://localhost:9000)

***Make sure to set it to local usage***