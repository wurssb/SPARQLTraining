# A Practical Overview of SPARQL


RDF (Resource Description Framework) is a framework for describing resources on the web. Unlike relational database models, which store data in tables and require structure formatting, RDF databases store data as triples, which facilitate data merging even if the underlying schemas differ.

**Triples: Subject – Predicate – Object**

In RDF data models, all information is represented as a collection of statements. Each statement has three components: a subject, a predicate (property), and an object. These statements are known as triples.

For example

![](images/london_capitalof_unitedkingdom.png)

In this example the Subject is: London; the Predicate (property) is CapitalOf, and the Object is United Kingdom.

An important feature of RDF databases is that each element in a triple has a unique identifier, each has its own URI/URL, which can be abbreviated as prefixed names.
For instance:
The predicate CapitalOf could have the URL: **<http://www.example.nl/countries/CapitalOf\>**
Once the URL is assigned to the prefix "gbol", it can be abbreviated as gbol:CapitalOf (see the box below).

Triples contain relationships and each of the three components of a triple can itself be used in other triples, which could then lead to triples being related to one another.

RDF databases are queried by the standardized language SPARQL (Sparql Protocol and RDF Query Language). SPARQL syntax is similar to SQL, but it is specific to RDF data and uses HTTP as its communication protocol.


**Structure of a SPARQL query:**


``` SPARQL
PREFIX gbol:<http://gbol.life/0.1/>						

SELECT ?p ?o								

WHERE {
	?sample a gbol:Sample . 
    ?sample ?p ?o
    #it is very important to end
    #the statements by a point ' . '.
}

GROUP BY ...
HAVING ...
LIMIT ...
```

**PREFIX** <br>
Prefix declaration - In order to abbreviate URLs in the query

**SELECT** <br>
Result clause - What we want the query to return

**WHERE** <br>
Query pattern - Navigating through the triple store

**GROUP BY etc.** <br>
Aggregrates - Apply expressions over groups of solutions


To start with the SPARQL exercise, navigate to the `Endpoint` tab in the menu and select the `BASIC` endpoint.

**For the next exercise you need to open the SPARQL training image in your browser.
Check your command line terminal for the exact location of the SPARQL training image
(looks something like http://localhost:7300#).**

###For further information

- An introduction to the Resource Description Framework
	- http://www.dlib.org/dlib/may98/miller/05miller.html

- Resource description Framework (RDF) model and Syntax Specification
	- https://www.w3.org/TR/1999/REC-rdf-syntax-19990222/

- Querying using SPARQL
	- http://www.dataversity.net/introduction-to-sparql/
	- http://www.cambridgesemantics.com/semantic-university/sparql-by-example
	- https://www.w3.org/TR/rdf-sparql-query/ 