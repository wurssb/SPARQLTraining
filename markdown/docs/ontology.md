# Ontology

We are going to define the database using a more strictly defined structure (ontology). We have simplified this process by using a Java API code generator and a simplified way of writing an ontology. In this exercise we are going to start making a possible ontology for the following graph (with additional information).

![MonaLisa](./images/ontology/lisa.png)

## Using an ontology

Using an ontology in the correct manner is complex and can take a while to master. To reduce the level of complexity we have developed an API which can be used to convert a general ontology into an API for Java. Usng this approach the necessary checks are performed when creating and linking objects. Using this approach it is ensured by the GateKeeper that an ontology is used in a precise manner.

Developing an ontology which the API generator understands is shown in the following steps.

First we need an editor to develop an ontology. In this case we use Protege but any editor should suffice. When starting a new ontology in protege we need to set the ontology URL (e.g. [http://example.com]()).

![URI](./images/ontology/uri.png)

In the class overview you can create the necesarry classes that are required for your ontology. In here you can also create classes that are part of a different ontology such that you can use all information through the API.

![New class](./images/ontology/newClass.png)

By default it uses the Ontology IRI but you can modify this accordingly.

When the Class is created you should see it under **owl:Thing** which is the root of all ontologies since everything can be a Thing.

To specify what properties are allowed for a given class or subclass you need to add this information to the Annotations section.

![Annotations](./images/ontology/Annotations.png)

The code generator makes use of its own URI to define properties and you need to create a new **sibling property** and use the following URL: [http://empusa.org/0.1/propertyDefinitions]().

![propertieDefinitions](./images/ontology/propertyDefinitions.png)

When this property is created we can fill in all the specifications possible.
For example, when we have a Class person we can assume that a Person class should have the following properties (among others).

- First name
- Last name
- Place of Birth
- Date of Birth
- Email adress
- Knows another Person
- ORCID (a persistent digital identifier that distinguishes you from every other researcher) 

If we want to fill this all in it would look like as follow:

```
#* This is a comment line about a persons first name and it should be of type string
firstName xsd:String;
#* The persons last name
lastName xsd:String;
#* Place of birth (here we use the @: to link to another class in this ontology) and we do not always know where a person was born so it is optional (? = zero or one)
placeOfBirth @:Location?;
#* Email address (not everybody has an email adress)
email xsd:String?;
#* It can be a social person and knows someone (zero or more people)
knows :@Person*;
#* Scientific identifier (url)
orcid IRI?;
```

To explain it further:

```
#* is a comment line and can be used to describe the meaning of the next statement
```

The statement line that is used by the API can be used to describe many things but you first define the name of the predicate that should be used to link two objects (remember from SPARQL?).

For example:

<http://example.com/PersonA> <http://example.com/firstName> "James"

To describe this statement in the ontology you would use `firstName` followed by the type that is allowed to link to this predicate (e.g. String/integer/float/URL/date/..)

To make sure it is a string:

```
#* A persons first name
firstName xsd:String;
```

There is no special character (\*?+) added after xsd:String but only a closing statement ; 
This means that a person needs to have a name and only one name.

```
xsd:String? Zero or one element can be added
xsd:String* Zero or more elements can be added
xsd:String+ One or more elements can be added
```

You can also specify other elemenst beside just `Strings`, here is a complete overview:

```
xsd:String
IRI
@:OTHERCLASSNAME
type::TYPECLASSNAME
xsd:Double
xsd:Integer
xsd:Float
xsd:Long
xsd:DateTime
xsd:Date
xsd:PositiveInteger
xsd:Boolean
```

Each of the elements can be substituted with `?*+`

Another special feature that is the `http://empusa.org/0.1/EnumeratedValueClass`
This allows you to have a predicate that links to a specified subset of properties.

For example when you want to state in which country a person is located in you can either store this as a string or as a set of country URL's which are strictly defined beforehand.

Or when you have a person in a company and you would like to associate the correct job function to the person from only a acceptable list.

To stay in the Museum example, we will create a list of possible items that are found to be in a musea to make catagorizing more easily.

To do this you need to create a class directly under the owl:Thing class with the URI: 

`http://empusa.org/0.1/EnumeratedValueClass`

Within this class you create another class containing the possible properties for example for types of items in a museum we create the `MuseumType` class.
Inside this class we create many subclasses of types that can be found in a musea. For example: Clothing, Sword, Vase, Statue, Painting

It should look as follow:

![EnumeratedValueClass](./images/ontology/EnumeratedValueClass.png)

And the museum item will point to this particular class using:

```
#* The kind of item this is
museumType type::MuseumType
```

Now that most properties are connected it is time to save the document in the right format. It is best to save it using the Turtle Syntax, and save it as <FileName>.ttl.

## Generating the API!

To generate the JAVA api you need the code generator. The code generator (EmpusaCodeGen.jar) can be obtained from: [http://download.systemsbiology.nl/empusa/](http://download.systemsbiology.nl/empusa/).

`wget http://download.systemsbiology.nl/empusa/EmpusaCodeGen.jar`

The most straightforward usage of this generator is to execute the following:

`java -jar EmpusaCodeGen.jar -i ./example.ttl -o myApi`

This will create a java gradle environment in the myApi folder.
To start programming we use the Intellij environment but any other programming environment should suffice.

## Using the API!

To start working on the API the project can be opened by opening the `build.gradle` file inside the myApi folder.

![Open](./images/ontology/open.png)

Once you have selected the `build.gradle` file the environment should start and shows you the code base.

![](./images/ontology/codebase.png)

The `com.example.domain` contains all the classess and enumerated values that are used by the ontology / API.

To make use of the API we create a separate class environment and initialize an RDF database.

```
package nl.wur.ssb;

import com.example.domain.Person;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

public class ExampleBase {
    public static void main(String[] args) throws Exception {
        // Database environment
        Domain domain = new Domain("");
        // Creation of the person Bob
        Person bob = domain.make(com.example.domain.Person.class, "http://example.com/person/Bob");
    }
}
```

In this example we have started a database environment using the `Domain` object. This can be a memory store using "" or a disk store using a folder path. It can also load an already existing RDF file and use the API against it or complement with additional data.

Secondly we have also created a person called Bob. all possible options available for bob can easily be requested by using `bob.<tab>`.

![](./images/ontology/bobOptions.png)

As you can see you can also request the properties from Bob. This is particular useful when you want to obtain the information again later on in the code without having to write a specific SPARQL query (which of course is also an option).

Evaluation of the properties is performed when the linkage between different classes is established.

To save the database to a local RDF file you can use the save command:

```
domain.save("result.ttl");
```

## Querying the domain resource
If you want to perform some complex queries from within the code base you can use the query functionality. This is achieved as follow:

```
public class ExampleBase {
    public static void main(String[] args) throws Exception {
        // Database environment
        Domain domain = new Domain("");
        Person bob = domain.make(com.example.domain.Person.class, "http://example.com/person/Bob");

        Iterable<ResultLine> results = domain.getRDFSimpleCon().runQuery("myQuery.sparql", false);
        results.forEach(result -> {
            System.out.println(result.getIRI("s"));
        });
    }
}
```

The myQuery.sparql file contains the query and is located in `./src/main/resources/queries/` in the same directory you need a file called header.txt (can be empty) in which you can store all the prefixes that you need for the queries you want to perform.

Content of myQuery.sparql:

```
SELECT *
WHERE { ?s ?p ?o }
```

Instead of `result.getIRI("s")` you can replace it by a large variety of different types (string, int etc..) depending on what you expect to give you more control on what should be achieved.


### Finishing the picture


![MonaLisa](./images/ontology/lisa.png)

Can you finalize the ontology to be able to store the information and perhaps add some additional information by a *Bob* or an *Alice* you may know? (e.g. Age / height / E-mail / etc...). If you happen to have an interesting dataset that you would like to try and convert feel free to do so!