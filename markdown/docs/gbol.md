To enable interoperability of genome annotations, we have developed the Genome Biology Ontology Language (GBOL). GBOL is provenance centered and provides a consistent representation of genome derived automated predictions linked to the dataset-wise and element-wise provenance of predicted elements. GBOL is modular in design, extendible and is integrated with existing ontologies. Interoperability of linked data can only be guaranteed through the application of tools that provide the means for a continuous validation of generated linked data. Genome wide large scale functional analyses can then easily be achieved using SPARQL queries. Additionally, modules have been developed to serialize the linked data (RDF) and to further annotate genomes.

For more information about the ontology  and about the annotation platform see see [gbol.life](http://gbol.life) and [sapp](http://sapp.gitlab.io). All genomes used in this tutorial are converted and annotated by SAPP.

![](./images/overview.svg)
An overview of the GBOL ontology

## Querying GBOL

In the top right `Choose repository` select GBOL. Then select `SPARQL` on the left to obtain the SPARQL interface from GraphDB.

**Prefixes used:**

    PREFIX gbol:<http://gbol.life/0.1/>


???+ note "Obtain all samples present in this dataset"

    GBOL stores each genome as a sample under the gbol:Sample class type.
    
	```SPARQL
	    SELECT DISTINCT ?sample
	    WHERE {
	       ?sample a gbol:.... . 
	    }
	```

	??? warning "Answer"
	
		```SPARQL
		    SELECT DISTINCT ?sample
		    WHERE {
		       ?sample a gbol:Sample . 
		    }
		```
			
		Selects all the samples that are of type gbol:Sample.
		
		There are 10 distinct samples in this database. Please note that queries are capital sensitive, if written gbol:sample (small ‘g’) no results are retrieved. Also, remember to include the ‘.’ to break the line; otherwise, the query will not run or cause errors.
		
		
		* gbol:GCA_900119705.1/sample
		* gbol:GCA_900116935.1/sample
		* gbol:GCA_900119915.1/sample
		* gbol:GCA_900119315.1/sample
		* gbol:GCA_900119785.1/sample
		* gbol:GCA_900116045.1/sample
		* gbol:GCA_900119775.1/sample
		* gbol:GCA_900116695.1/sample
		* gbol:GCA_900119755.1/sample
		* gbol:GCA_900119765.1/sample 


???+ note "What predicates can be associated with a sample"

    In graphdb you can click on your results but not every SPARQL endpoint supports this. You can also retrieve the properties belonging to a subject using a ?subject ?predicate ?object statement.
    
	```SPARQL
	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?predicate
	WHERE {
	    ?sample a gbol:Sample .
	    ?sample ?... ?....
	}
	```

	??? warning "Answer"
	
		```SPARQL
		PREFIX gbol:<http://gbol.life/0.1/>
		SELECT DISTINCT ?predicate
		WHERE {
		    ?sample a gbol:Sample .
		    ?sample ?predicate ?oobject .
		}
		```
			
		We preselect all samples as used in the previous query and in the next statement we ask for all predicates and all objects. In larger databases this can quickly generate a lot of data.
		
		* rdf:type
		* gbol:xref
		* gbol:chromosome
		* gbol:strain
		* gbol:isolationSource
		* gbol:isolate
		* gbol:plasmid 

???+ note "What is the source where this sample came from?"

	```SPARQL
    PREFIX gbol:<http://gbol.life/0.1/>
    SELECT DISTINCT ?source
    WHERE {
       ?sample a gbol:Sample .
       ?sample gbol:.... ?source.
    }
	```

	??? warning "Answer"
	
		```SPARQL
	    PREFIX gbol:<http://gbol.life/0.1/>
	    SELECT DISTINCT ?source
	    WHERE {
	       ?sample a gbol:Sample .
	       ?sample gbol:isolationSource ?source.
	    }
		```
		
		* Kopanisti cheese
		* soil
		* Clinical
		* environmental

 
???+ note "What are the predicates associated with the object DnaObject?"

	Various dnaobjects, Contig/Scaffold/Chromosomes are connected to the sample via gbol:sample. But what other predicates are connected to this dnaobject?
	
	```SPARQL
	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?predicate
	WHERE {
		?sample a gbol:Sample .
		?contig gbol:... ?sample .
		?contig ?... ?... .
	}
	```
	
	??? warning "Answer"
	
		```SPARQL
		PREFIX gbol:<http://gbol.life/0.1/>
		SELECT DISTINCT ?p
		WHERE {
			?sample a gbol:Sample .
			?contig gbol:sample ?sample .
			?contig ?p ?o .
		}
		```
		
		We start from a sample of which we ask which contigs or "dnaobjects" are connected to it. For each contig we then ask all the predictes.

		* rdf:type
		* [http://gbol.life/0.1/accession](http://gbol.life/0.1/accession)
		* [http://gbol.life/0.1/description](http://gbol.life/0.1/description)
		* [http://gbol.life/0.1/feature](http://gbol.life/0.1/feature)
		* [http://gbol.life/0.1/length](http://gbol.life/0.1/length)
		* [http://gbol.life/0.1/organism](http://gbol.life/0.1/organism)
		* [http://gbol.life/0.1/sample](http://gbol.life/0.1/sample)
		* [http://gbol.life/0.1/sequence](http://gbol.life/0.1/sequence)
		* [http://gbol.life/0.1/sequenceVersion](http://gbol.life/0.1/sequenceVersion)
		* [http://gbol.life/0.1/sha384](http://gbol.life/0.1/sha384)
		* [http://gbol.life/0.1/topology](http://gbol.life/0.1/topology)
		* [http://gbol.life/0.1/translTable ](http://gbol.life/0.1/translTable )



???+ note "What are the predicates associated with the object 'dna' for the genome "Legionella pneumophila"

	```SPARQL
	    PREFIX gbol:<http://gbol.life/0.1/>
	    SELECT DISTINCT ?predicate
	    WHERE {
	      	VALUES ?name { "Legionella ...." }
	      	?contig gbol:organism ?organism .
	    	?organism gbol:.... ?name .
	    	?contig ?.... ?.... .
	  	}
	```

	??? warning "Answer"
		```SPARQL
		    PREFIX gbol:<http://gbol.life/0.1/>
		    SELECT DISTINCT ?p
		    WHERE {
		      	VALUES ?name { "Legionella pneumophila" }
		      	?contig gbol:organism ?organism .
		    	?organism gbol:scientificName ?name .
		    	?contig ?p ?o .
		  	}
		```
	
		Answer: There are multiple distinct predicates. More imporantly is the use of VALUES where you can assign one or multiple values for a variable `?name` allowing you to quickly filter using direct matches.

???+ note "Getting the number of genes for all or a particular genome"

	To obtain the number of genes associated to a sample we need to follow a specific route through the database. First, obtain the contigs belonging with a sample. Then for each contig obtain the features attached to it which are of type Gene.
		
	```SPARQL
	   PREFIX gbol:<http://gbol.life/0.1/>
		SELECT ?sample ?name (COUNT(?gene) AS ?counts)
	    WHERE {
	      	# VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
	      	# You can remove the # before VALUES to only query a specific sample
	      	?contig gbol:organism ?organism .
	    	?organism gbol:scientificName ?name .
			?contig gbol:sample ?sample .
	    	?contig gbol:.... ?gene .
	    	?gene a gbol:.... . 
	  	} GROUP BY ?sample ?name
	```
	
	??? warning "Answer"

		```SPARQL
		PREFIX gbol:<http://gbol.life/0.1/>
		SELECT ?sample ?name (COUNT(?gene) AS ?counts)
		WHERE {
		    # VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
		    # You can remove the # before VALUES to only query a specific sample
		    ?contig gbol:organism ?organism .
		    ?organism gbol:scientificName ?name .
		    ?contig gbol:sample ?sample .
		    ?contig gbol:feature ?gene .
		    ?gene a gbol:Gene . 
		} GROUP BY ?sample ?name
		```
		
		You might have noticed that some of the samples are missing from the results.
		This is due to the fact that these genomes have not been annotated to a taxonomic level.
		Therefore it does not have the connection from the contig to an organism and its scientific name.


???+ note "Provenance of the gene prediction"
	
	To obtain the information about how a gene was found and what kind of provenance is attached to it (e.g. the additional information from a tool that is often ignored) the following query can be used
	
	```SPARQL
	PREFIX gbol: <http://gbol.life/0.1/>
	SELECT DISTINCT ?gene ?annot
	WHERE { 
		?contig a gbol:Contig .
		?contig gbol:feature ?gene .
		?gene a gbol:Gene .
		?gene gbol:locusTag ?locus .
		?gene gbol:provenance ?prov .
		?prov gbol:annotation ?annot .
	}
	```
	
	You can click on the ?annot result of a given gene (second column) to look into what the prediction tool gives for additional information.
	
	??? warning "Answer"
	
		*An example of a random protein in the GBOL database*
		
		```
		...life/0.1/cScore> 5.13
		...life/0.1/conf> 50.71
		...life/0.1/gcContent> 0.650 .
		...life/0.1/rScore> 0.47
		...life/0.1/rbsMotif> None .
		...life/0.1/rbsSpacer> None .
		...life/0.1/sScore> -5.01
		...life/0.1/score> 0.12
		...life/0.1/startType> TTG .
		...life/0.1/tScore> -5.5
		...life/0.1/uScore> -0.79
		```


???+ note "Getting the number of proteins for all or a particular genome"

	This is an extension on the question from genes. How to go from gene to protein?
	
	```SPARQL
	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT ?sample ?name (COUNT(?protein) AS ?counts)
	WHERE {
	  	# VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
		?contig gbol:sample ?sample .
    	?contig gbol:.... ?gene .
    	?gene a gbol:.... . 
		?gene gbol:.... ?mrna .
		?mrna gbol:feature ?cds .
		?cds gbol:.... ?protein .
	} GROUP BY ?sample ?name
	```
	
	??? warning "Answer"

		```SPARQL
		   PREFIX gbol:<http://gbol.life/0.1/>
			SELECT ?sample ?name (COUNT(?protein) AS ?counts)
		    WHERE {
		      	# VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
				?contig gbol:sample ?sample .
		    	?contig gbol:feature ?gene .
		    	?gene a gbol:Gene . 
		    	?gene gbol:transcript ?mrna .
		    	?mrna gbol:feature ?cds .
		    	?cds gbol:protein ?protein .
		  	} GROUP BY ?sample ?name
		```
		You can comment/uncomment the VALUES line to only search in a particular genome or in all.
		
		* <http://gbol.life/0.1/GCA_900119775.1.embl.gz/sample> 3082
		* <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> 3125
		* <http://gbol.life/0.1/GCA_900116045.1.embl.gz/sample> 2535
		* <http://gbol.life/0.1/C4_1_scaffolds.filter.fasta> 5473
		* <http://gbol.life/0.1/D19_scaffolds.filter.fasta> 31524



		We perform a grouping approach (`GROUP BY`) to group the proteins per sample and name (as the name alone might not be unique enough but it is more human readable). This results in the `sampleIRI` combined with the organism name and the total amount of proteins available for a given genome.
		
		To perform this query for a specific organism we can again use the VALUES approach to put in one or more URL's to select the samples of interest.

???+ note "Retrieval of functional information from a protein"

	Additional protein features such as PFAM domains, EC numbers, etc... are stored as external references to protein types.

	```SPARQL
	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?sample ?db ?accession
	WHERE {
	    # VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
	    ?contig gbol:sample ?sample .
	    ?contig gbol:feature ?gene .
	    ?gene a gbol:Gene . 
	    ?gene gbol:transcript ?mrna .
	    ?mrna gbol:feature ?cds .
	    ?cds gbol:protein ?protein .
	    ?protein gbol:.... ?xref .
	    ?xref gbol:.... ?db .
	    ?xref gbol:.... ?accession .
	}
	```

	??? warning "Answer"
		```SPARQL
		PREFIX gbol:<http://gbol.life/0.1/>
		SELECT DISTINCT ?sample ?db ?accession
		WHERE {
		    # VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
		    ?contig gbol:sample ?sample .
		    ?contig gbol:feature ?gene .
		    ?gene a gbol:Gene . 
		    ?gene gbol:transcript ?mrna .
		    ?mrna gbol:feature ?cds .
		    ?cds gbol:protein ?protein .
		    ?protein gbol:xref ?xref .
		    ?xref gbol:db ?db .
		    ?xref gbol:accession ?accession .
		}
		```


		With this query we can retrieve all external references attached to a protein. In this case only EC numbers are available as only a simple EMBL file conversion was performed and loaded into this triple store. However when multiple annotation tools might have been applied to this dataset it could be important to track the origin of this annotation. For example, to know what application was used for this `xref` you can uncomment the bottom section and add `?name` to the `SELECT` statement to see that this was due to the `Conversion module`. 


???+ note "Using the FILTER and REGEX option to identify proteins with something related to alcohol (case insensitive)"

	```SPARQL
	PREFIX gbol:<http://gbol.life/0.1/>
	SELECT DISTINCT ?gene ?product
	WHERE {
	    VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
	    ?contig gbol:sample ?sample .
	    ?contig gbol:feature ?gene .
	    ?gene a gbol:Gene . 
	    ?gene gbol:transcript ?mrna .
	    ?mrna gbol:feature ?cds .
	    ?cds gbol:protein ?protein .
	    ?cds gbol:.... ?product .
	    ....(....(?product,"....","i"))
	}
	```

	??? warning "Answer"
		```SPARQL
		PREFIX gbol:<http://gbol.life/0.1/>
		SELECT DISTINCT ?gene ?product
		WHERE {
		    VALUES ?sample { <http://gbol.life/0.1/GCA_900119755.1.embl.gz/sample> }
		    ?contig gbol:sample ?sample .
		    ?contig gbol:feature ?gene .
		    ?gene a gbol:Gene . 
		    ?gene gbol:transcript ?mrna .
		    ?mrna gbol:feature ?cds .
		    ?cds gbol:protein ?protein .
		    ?cds gbol:product ?product .
		    FILTER(REGEX(?product,"alcohol","i"))
		}
		```

		
		Here we perform a FILTER on the `?product` which contains strings of product names. Using REGEX we can perform regular expressions such that `al.ohol` would yield at least the same result or more. 
		
		* http://gbol.life/0.1/GCA_900119755.1.embl.gz/LT632615/generatedGene/c663489-664535 **NADP-dependent alcohol dehydrogenase C 2**
		* http://gbol.life/0.1/GCA_900119755.1.embl.gz/LT632615/generatedGene/c2527007-2528020 **Zinc-type alcohol dehydrogenase-like protein SA1988**
		* http://gbol.life/0.1/GCA_900119755.1.embl.gz/LT632615/generatedGene/2906389-2907375 **Alcohol dehydrogenase**