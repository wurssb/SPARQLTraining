#!/usr/bin/env bash
echo "Start this docker image with the following command:"

echo "##############################################################"
echo "#docker run -p 8080:80 -p 7300:7200 wurssb/sparqltraining#"
echo "##############################################################"

echo "Once started... These are important web addresses"

echo "###############################################"
echo "SPARQL training image at http://localhost:7300#"
echo "###############################################"

echo "#######################################################"
echo "SPARQL training documentation at http://localhost:8080#"
echo "#######################################################"

