FROM openjdk:8-jdk

# Build time arguments
ARG version=8.6.1
ARG edition=free

ENV GRAPHDB_PARENT_DIR=/opt/graphdb
ENV GRAPHDB_HOME=${GRAPHDB_PARENT_DIR}/home

ENV GRAPHDB_INSTALL_DIR=${GRAPHDB_PARENT_DIR}/dist

ADD graphdb-free-${version}-dist.zip /tmp

RUN mkdir -p ${GRAPHDB_PARENT_DIR} && \
    cd ${GRAPHDB_PARENT_DIR} && \
    unzip /tmp/graphdb-free-${version}-dist.zip && \
    rm /tmp/graphdb-free-${version}-dist.zip && \
    mv graphdb-${edition}-${version} dist && \
    mkdir -p ${GRAPHDB_HOME}

ENV PATH=${GRAPHDB_INSTALL_DIR}/bin:$PATH

EXPOSE 7200

##############################################################################
##### ADDING DATA ######

##############################################################################
# Adding Basic data
ADD data/basic /root/basic
RUN /opt/graphdb/dist/bin/loadrdf --configFile root/basic/basic.ttl --mode parallel /root/basic

##############################################################################
# Adding GBOL data
ADD data/genomes /root/genomes
RUN /opt/graphdb/dist/bin/loadrdf --configFile root/genomes/gbol.ttl --mode parallel /root/genomes

##############################################################################
# Adding Wikidata Wageningen
ADD data/wikidata/ /root/wikidata
RUN /opt/graphdb/dist/bin/loadrdf --configFile root/wikidata/Q1305.ttl --mode parallel /root/wikidata/

##############################################################################
# Adding Enipedia
ADD data/enipedia/ /root/enipedia
RUN /opt/graphdb/dist/bin/loadrdf --configFile root/enipedia/enipedia.ttl --mode parallel /root/enipedia/

##############################################################################
# Adding UniProt
#ADD data/uniprot/ /root/uniprot
#RUN /opt/graphdb/dist/bin/loadrdf --configFile root/uniprot/uniprot.ttl --mode parallel /#root/uniprot/
######


##############################################################################
# Running the manual webserver
EXPOSE 80
RUN apt-get update && apt-get -y install apache2

ADD markdown/site /var/www/html/
ADD message.sh /message.sh
RUN chmod +x /message.sh

ENTRYPOINT /message.sh && /usr/sbin/apache2ctl start && /opt/graphdb/dist/bin/graphdb #&& /bin/bash
