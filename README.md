# SPARQL Training

Docker image containing graphdb and a markdown documentation website for training purposes

## Getting Started

To start the docker training environment use the following command

```
docker run -p 8080:80 -p 7300:7200 wurssb/sparqltraining
```

After running this command your system will start up a docker image of this sparql training
Follow the both the http://localhost URL's to start up the tutorial and the SPARQL database.

To manage docker images more easily the following docker image can be started

```
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
```


### Prerequisites

What things you need to run the Docker and how to install them

MacOSX:
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install Docker
```

Linux OS:
```
sudo apt install docker.io
```


## Dependencies

* [GraphDB](http://graphdb.ontotext.com) - Semantic Graph Database (Download GraphDB Free Edition)
* MarkDown
* [Docker](https://www.docker.com) - Containerization platform

## Contributing

Feel free to contribute to the documentation or datasets that are currently used for training purposes

## Authors

Melanie van den Bosch & Jasper Koehorst.

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* GraphDB for allowing me to distribute the free version for training purposes within this docker image